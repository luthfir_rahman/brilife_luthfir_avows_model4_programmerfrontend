import React from "react";
import Loadable from "react-loadable";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
} from "react-router-dom";
import clsx from "clsx";
import { useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import People from "@material-ui/icons/People";
import Assessment from "@material-ui/icons/Assessment";
import Group from "@material-ui/icons/Group";

import useStyles from "./styles";

const Loading = () => <div>Loading...</div>;
const Home = Loadable({
  loader: () => import("./components/Home"),
  loading: Loading,
});
const Agen_Home = Loadable({
  loader: () => import("./components/form/agen/Agen"),
  loading: Loading,
});

function App() {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };
  return (
    <Router>
      <div className={classes.root}>
        <CssBaseline />
        <AppBar
          position="fixed"
          className={clsx(classes.appBar, {
            [classes.appBarShift]: open,
          })}
        >
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              className={clsx(classes.menuButton, open && classes.hide)}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" noWrap>
              <NavLink className={classes.NavLink} to="/">
                BRILife
              </NavLink>
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="left"
          open={open}
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <div className={classes.drawerHeader}>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === "ltr" ? (
                <ChevronLeftIcon />
              ) : (
                <ChevronRightIcon />
              )}
            </IconButton>
          </div>
          <Divider />
          <List>
            <NavLink className={classes.NavMenu} to="/Agen">
              <ListItem button key="">
                <ListItemIcon>
                  <People />
                </ListItemIcon>
                <ListItemText primary="Data Agen" />
              </ListItem>
            </NavLink>
            <NavLink className={classes.NavMenu} to="/StukturAgen">
              <ListItem button key="">
                <ListItemIcon>
                  <Group />
                </ListItemIcon>
                <ListItemText primary="Data Struktur Agen" />
              </ListItem>
            </NavLink>
          </List>
          <Divider />
          <List>
            <NavLink className={classes.NavMenu} to="/LaporanAgen">
              <ListItem button key="">
                <ListItemIcon>
                  <Assessment />
                </ListItemIcon>
                <ListItemText primary="Laporan Agen" />
              </ListItem>
            </NavLink>
          </List>
        </Drawer>
        <main
          className={clsx(classes.content, {
            [classes.contentShift]: open,
          })}
        >
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/Agen" component={Agen_Home} />
          </Switch>
        </main>
      </div>
    </Router>
  );
}

export default App;
