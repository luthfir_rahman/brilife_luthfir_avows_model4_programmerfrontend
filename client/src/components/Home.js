import React from "react";
import { Paper } from '@material-ui/core';

import useStyles from "./styles";

const Home = () => {
  const classes = useStyles();
  return (
    <Paper className={classes.paper}>
      <h1>Home</h1>
    </Paper>
  );
};

export default Home;
