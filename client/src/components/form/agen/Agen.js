import React, { useState, useEffect} from 'react';
import { TextField, Button, Typography, Paper, Select, InputLabel } from '@material-ui/core';
import Axios from 'axios';

import useStyles from "./styles";

const Agen = ({ currentId, setCurrentId }) => {
    const [postData, setPostData] = useState({
        no_lisensi: '', nama_agen: '',  id_agen_level: '', status: '', status_tgl: '', wilah_kerja: '',
    });
  const classes = useStyles();
  const [agenlevelList , setagenlevelList] =useState([]);

  useEffect(() => {
    Axios.get('http://localhost:5000/api/datlevel').then((response)=>{
      console.log(response.data);
      setagenlevelList(response.data);
    });
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    
    Axios.post('http://localhost:5000/api/inagen', postData).then(() => {
      alert("Brhasil Menyimpan data!");
    });
    console.log(postData);
    
    clear();
}

const clear = () => {
    
    setPostData({no_lisensi: '', nama_agen: '',  id_agen_level: '', status: '', status_tgl: '', wilah_kerja: ''});  
}

  return (
    <Paper className={classes.paper}>
        <form autoComplete="off" noValidate className={`${classes.root} ${classes.form}`} onSubmit={handleSubmit}>
            <Typography variant="h6">{ currentId ? 'Editing' : 'Creating' } Agens</Typography>
            <TextField name="no_lisensi" variant="outlined" label="Nomor Lisensi" fullWidth value={postData.no_lisensi} onChange={(e) => setPostData({ ...postData, no_lisensi: e.target.value })} />
            <TextField name="nama_agen" variant="outlined" label="Nama Agen" fullWidth value={postData.nama_agen} onChange={(e) => setPostData({ ...postData, nama_agen: e.target.value })} />
            <InputLabel variant="outlined">Level Agen</InputLabel>
            <Select name="id_agen_level" variant="outlined" fullWidth value={postData.id_agen_level} onChange={(e) => setPostData({ ...postData, id_agen_level: e.target.value })}>
                {agenlevelList.map((val) => {
                  return <option value={val.id} >{val.level}</option>
                })}
            </Select>
            <TextField name="status" variant="outlined" label="Status" fullWidth value={postData.status} onChange={(e) => setPostData({ ...postData, status: e.target.value })} />
            <TextField type="date" name="status_tgl" variant="outlined" label="Status Tanggal" fullWidth value={postData.status_tgl} onChange={(e) => setPostData({ ...postData, status_tgl: e.target.value })} />
            <TextField name="wilah_kerja" variant="outlined" label="Wilah Kerja" fullWidth value={postData.wilah_kerja} onChange={(e) => setPostData({ ...postData, wilah_kerja: e.target.value })} />
            <Button className={classes.buttonSubmit} variant="contained" color="primary" size="large" type="submit" fullWidth>Submit</Button>
            <Button variant="contained" color="secondary" size="small" onClick={clear} fullWidth>Clear</Button>
        </form>
    </Paper>
  );
};

export default Agen;
