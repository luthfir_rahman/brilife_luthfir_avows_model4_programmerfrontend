import express from 'express';
import bodyParser from 'body-parser';
import mysql from 'mysql';
import cors from 'cors';

const app = express();
const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '10Agustus1995@',
    database: 'dbbril_agen',
});

db.connect((err) => {
    if (!err) {
        console.log("database terkoneksi");
    }else{
        console.log("database gagal terkoneksi \n error :" + JSON.stringify(err, undefined, 2));
    }
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());


app.post("/api/inagen", (req, res) => {
    const no_lisensi = req.body.no_lisensi;
    const nama_agen = req.body.nama_agen;
    const id_agen_level = req.body.id_agen_level;
    const statusw = req.body.status;
    const status_tgl = req.body.status_tgl;
    const wilayah_kerja = req.body.wilayah_kerja;

    const sqlInsertAgen = "INSERT INTO dbo.agen (no_lisensi, nama_agen, id_agen_level, status, status_tgl, wilayah_kerja) VALUES(?,?,?,?,?,?)";
    db.query(sqlInsertAgen, [no_lisensi, nama_agen, id_agen_level, statusw, status_tgl, wilayah_kerja], (err, result) => {
        console.log(err);
    });
});

app.get("/api/datlevel", (req, res) => {
    const sqlDataAgen = "SELECT * FROM `dbo.agen_level` ORDER BY urutan ASC";
    db.query(sqlDataAgen, (err, rows) => {
        if (!err) {
            res.send(rows)
        }else{
            console.log(err)
        }
    })
})

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server berjalan di Port: ${PORT}`));